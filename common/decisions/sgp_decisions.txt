decision_joining_goa = {
	owned_planets_only = yes
	sound = no_sound
	icon = decision_luxuries

	resources = {
		category = decisions
		cost = {
			sr_goauld = 1
		}
	}
	
	potential = {
		owner = { has_country_flag = parasite_goa }
        planet = { has_humanoids = yes }			
		NOT = { has_modifier = pm_joining_goa }
	    }
	
	effect = {
		custom_tooltip = decision_joining_goa_custom
		add_modifier = { modifier = "pm_joining_goa" days = 360 }
		random_pop = {
			limit = { is_species_class = HUM }
			kill_pop = yes
		}
		create_pop = {
			species = owner_main_species
		}
	}
	
	ai_weight = {
		weight = 0
	}
}