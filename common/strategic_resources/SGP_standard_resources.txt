### Larvae - Get from broodpools
   sr_larvae ={
      tradable = no
      max = 1000
      deficit_modifier = sr_larvae_deficit
      ai_weight = { weight = 15 }
      ai_wants = { base = 500 }
      prerequisites = { }
   }

### Goauld - Get from Jaffa and Larvae
   sr_goauld ={
      tradable = no
      max = 1000
      deficit_modifier = sr_goauld_deficit
      ai_weight = { weight = 15 }
      ai_wants = { base = 500 }
      prerequisites = {  }
   }


### Trained Officers
   sr_crew ={
      tradable = no
      max = 10000
      fixed_max_amount = yes
      deficit_modifier = sr_crew_deficit #### found in static modifiers
      ai_weight = { weight = 5 }
      ai_wants = { base = 500 }
   }