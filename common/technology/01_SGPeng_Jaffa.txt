
tech_Jaffa_start = {
	cost = 0
	area = physics
	tier = 0
	category = { computing }
	start_tech = yes

	potential = {
		has_country_flag = Free_Jaffa_Nation 
	}
}