##################
### TECH COSTS ###
##################
#If you change any of these, use replace in files so the values are the same across all files

@tier1cost1 = 500
@tier1cost2 = 640
@tier1cost3 = 780

@tier2cost1 = 1000
@tier2cost2 = 1500
@tier2cost3 = 2000

@tier3cost1 = 3000
@tier3cost2 = 4000
@tier3cost3 = 5000

@tier4cost1 = 6000
@tier4cost2 = 8000
@tier4cost3 = 10000

@tier5cost1 = 14000
@tier5cost2 = 17000
@tier5cost3 = 20000

####################
### TECH WEIGHTS ###
####################

@tier1weight1 = 100
@tier1weight2 = 95
@tier1weight3 = 90

@tier2weight1 = 85
@tier2weight2 = 75
@tier2weight3 = 70

@tier3weight1 = 65
@tier3weight2 = 60
@tier3weight3 = 50

@tier4weight1 = 45
@tier4weight2 = 40
@tier4weight3 = 35

@tier5weight1 = 30
@tier5weight2 = 25
@tier5weight3 = 20

tech_Tauri_start = {
	cost = 0
	area = physics
	tier = 0
	category = { computing }
	start_tech = yes

	potential = {
		has_country_flag = tauri_empire
	}
}

tech_Goauld_start = {
	cost = 0
	area = physics
	tier = 0
	category = { computing }
	start_tech = yes

	potential = {
		OR = {
		
		has_country_flag = amaterasu_empire
		has_country_flag = anubis_empire
		has_country_flag = apophis_empire
		has_country_flag = ares_empire
		has_country_flag = baal_empire
		has_country_flag = bastet_empire
		has_country_flag = cronus_empire
		has_country_flag = hathor_empire
		has_country_flag = heruur_empire
		has_country_flag = ishkur_empire
		has_country_flag = isis_empire
		has_country_flag = kali_empire
		has_country_flag = marduk_empire
		has_country_flag = morrigan_empire
		has_country_flag = nirrti_empire
		has_country_flag = olokun_empire
		has_country_flag = osiris_empire
		has_country_flag = ra_empire
		has_country_flag = ramius_empire
		has_country_flag = sokar_empire
		has_country_flag = svarog_empire
		has_country_flag = yu_huang_empire
		has_country_flag = tok_ra_empire
		}
	}	
}