# Very Rare 	= 1
# Rare 			= 5
# Uncommen 		= 10
# Common 		= 20
# Prevelent		= 40

 pm_stargate_network_milkyway = {
 	spawn_chance = {
 		modifier = {
 			add = 40
 			OR = {
 				# Atmosphere / Life
 				is_planet_class = pc_continental
 				is_planet_class = pc_tropical
 				is_planet_class = pc_ocean
 				is_planet_class = pc_arctic
 				is_planet_class = pc_alpine
 				is_planet_class = pc_tundra
 				is_planet_class = pc_desert
 				is_planet_class = pc_savannah
				is_planet_class = pc_arid
				is_planet_class = pc_nuked
				is_planet_class = pc_city 
 			}
 		}

 		modifier = {
 			add = 100
 			is_planet_class = pc_gaia
 		}

 		modifier = {
 			factor = 0
 			has_planet_modifier = pm_stargate_network_milkyway
		 }
		 modifier = {
			 factor = 0
			 solar_system = { any_system_planet = { has_planet_modifier = pm_stargate_network_milkyway } }
		 }
 	}
	
 	modifier = "pm_stargate_network_milkyway"
 }

#  pm_stargate_network_pegasus = {
# 	spawn_chance = {
# 		modifier = {
# 			add = 40
# 			OR = {
# 				# Atmosphere / Life
# 				is_planet_class = pc_continental
# 				is_planet_class = pc_tropical
# 				is_planet_class = pc_ocean
# 				is_planet_class = pc_arctic
# 				is_planet_class = pc_alpine
# 				is_planet_class = pc_tundra
# 				is_planet_class = pc_desert
# 				is_planet_class = pc_savannah
# 			   is_planet_class = pc_arid
# 			   is_planet_class = pc_nuked
# 			   is_planet_class = pc_city 
# 			}
# 		}

# 		modifier = {
# 			add = 100
# 			is_planet_class = pc_gaia
# 		}

# 		modifier = {
# 			factor = 0
# 			has_planet_modifier = pm_stargate_network_pegasus
# 		}
# 		modifier = {
# 			factor = 0
# 			solar_system = { any_system_planet = { has_planet_modifier = pm_stargate_network_pegasus } }
# 		}
# 	}
   
# 	modifier = "pm_stargate_network_pegasus"
# }