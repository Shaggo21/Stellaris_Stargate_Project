## Possible Deposit Variables ##
# resources, resource and amount
# potential trigger (planet scope)
# planet_modifier - applied to planet only when deposit blocker has been cleared
# constant_modifier - applied to planet always
# blocker = <key/any/none> - default any
# station = station class in orbit to gather

@high = 16
@med = 8
@low = 4

@high_rare = 2
@med_rare = 1
@low_rare = 0.5

###Planet Deposits

d_stargate_milkyway = {
	is_for_colonizeable = yes
	icon = d_stargate_milkyway

	potential = {
		has_modifier = pm_stargate_network_milkyway
	}

	# constant_modifier = {
	# 	job_researcher_add = 3
	# 	job_merchant_add = 2
	# 	trade_value_mult = 0.10
	# }

	planet_modifier = {
		planet_max_districts_add = +1
		job_researcher_add = 3
		job_merchant_add = 2
		trade_value_mult = 0.10
	}

	drop_weight = {
		weight = 0
	}
}

