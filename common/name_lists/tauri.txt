tauri = {
	randomized = no

	ship_names = {
		generic = { }	
		constructor = { }
		colonizer = { }
		science = { }
		transport = { }
	}

	fleet_names = {
		sequential_name = "%O% Fleet"
	}

	army_names = {
		defense_army = {
			sequential_name = "%O% Infantry Divison"
		}
		assault_army = {
			sequential_name = "%O% Infantry Strike Force"
		}
		slave_army = {
			sequential_name = "%O% Indentured Defence Force"
		}
		clone_army = { 
			sequential_name = "%O% Clone Defence Force"
		}
	}

	planet_names = {
		generic = {
			names = { 
			}
		}
	}

	character_names = {
		names1 = {
			weight = 40
			first_names_male = { Liam Noah William James Logan Benjamin Mason Elijah Oliver Jacob Lucas Michael Alexander Ethan Daniel Matthew Aiden Henry Joseph Jackson Samuel Sebastian David Carter Wyatt Jayden John Owen Dylan Luke Gabriel Anthony Isaac Grayson Jack Julian Levi Christopher Joshua Andrew Lincoln Mateo Ryan Jaxon Nathan Aaron Isaiah Thomas Charles Caleb Josiah Christian Hunter Eli Jonathan Connor Landon Adrian Asher Cameron Leo Theodore Jeremiah Hudson Robert Easton Nolan Nicholas Ezra Colton Angel Brayden Jordan Dominic Austin Ian Adam Elias Greyson Jose Ezekiel Carson Jace Cooper Jason 
			}
			first_names_female = { Emma Olivia Ava Isabella Sophia Charlotte Mia Amelia Evelyn Abigail Emily Elizabeth Mila Ella Avery Sofia Camilla Aria Scarlett Victoria Luna Grace Chloe Penelope Layla Riley Zoey Nora Lily Eleanor Hannah Lillian Aubrey Ellie Stella Natalie Zoe Leah Hazel Violet Aurora Savannah Audrey Bella Claire Skylar Lucy Paisley Everly Anna Caroline Nova Emilia Samantha Maya Willow Naomi Aaliyah Elena Sarah Ariana Gabriella Alice Madelyn Cora Ruby Eva Serenity Adeline Hailey Gianna Valentia Isla Eliana Ivy Sadie Piper Lydia Alexy Josephine Julia Delilah Arianna Vivian Sophie Madeline
			}
			second_names = { Smith Johnson Williams Brown Jones Miller Davis Garcia Rodriguez Wilson Martinez Anderson Taylor Thomas Hernandez Moore Martin Jackson Thompson White Lopez Lee Gonzalez Harris Clark Lewis Robinson Walker Perez Hall Young Allen Sanchez Wright King Scott Green Baker Adams Nelson Hill Ramirez Campbell Mitchell Roberts Carter Phillps Evans Turner Torres Parker Collins Edwards Stewart Flores Morris Nguyen Murphy Rivera Cook Rogers Morgan Peterson Cooper Reed Bailey Bell Gomez Kelly Howard Ward Cox Diaz Richardson Wood Watson Brooks Bennet Gray James Reyes Cruz Hughes Price Myers Long 
			}
			regnal_first_names_male = {
			}
			regnal_first_names_female = {
			}
			regnal_second_names = {
			}
		}
	}
}