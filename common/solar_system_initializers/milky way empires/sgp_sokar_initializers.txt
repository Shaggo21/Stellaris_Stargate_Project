@distance = 30
@base_moon_distance = 10

sokar_homeworld = { 
	name = "Imhet" #Underworld region associated with Sokar
	class = "sc_m"
	flags = { }
	init_effect = { }
	usage = custom_empire
	max_instances = 1
	planet = {
		name = "Imhet"
		class = pc_m_star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	
	planet = {
		name = "Neter-khertet" # "Divine place underworld", in egyption mith.
		class = "pc_molten"
		orbit_distance = 25
		orbit_angle = 60
		size = 15
		has_ring = no
	}
	
	planet = {
		name = "Amhet" # Comes from other way to say Imhet
		class = "pc_barren"
		orbit_distance = 25
		orbit_angle = 60
		size = 8
		has_ring = no
	}

	planet = {
		name = "Delmak"
		class = pc_desert # Should be an ecumenopolis?
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		starting_planet = yes
		deposit_blockers = none
		modifiers = none 
		init_effect = { prevent_anomaly = yes set_planet_flag = planet_sokar }
		init_effect = {
			set_global_flag = sokar_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = sokar_empire } } }
				create_species = { 
				    name = "Dominion of Sokar" 
				    class = SOK
				    portrait = goauld
				    homeworld = THIS
				    traits = {
						trait =	"trait_extremely_adaptive"
						trait =	"trait_decadent"
						trait =	"trait_weak"
						trait =	"trait_repugnant"
						trait =	"trait_rapid_breeders"
						ideal_planet_class = "pc_desert"
					}
				}
				last_created_species = { save_global_event_target_as = sokarSpecies }
				create_country = {
					name = "NAME_sokar_dominion"
					ship_prefix = "RAA"
					type = default
					origin = "default"
					ignore_initial_colony_error = yes
					civics = { civic = "civic_stargate_sokar" civic = "civic_corvee_system" }
					authority = auth_dictatoric
					name_list = "ART2"
					ethos = { ethic = "ethic_fanatic_authoritarian" ethic = "ethic_pacifists" }
					species = event_target:sokarSpecies
					flag = {
						icon =	{ category =	"stargate" file =	"sokar.dds" }
						background =	{ category =	"backgrounds" file =	"v.dds" }
						colors =	{ "black" "black" "null" "null" }
					}
					effect = {
						set_graphical_culture = tauri_01
						set_country_flag = sokar_empire
						save_global_event_target_as = sokar_empire
					}
				}
				set_owner = event_target:sokar_empire
			}
			add_modifier = { modifier = pm_stargate_network_milkyway }
			# add_modifier = { modifier = pm_glyph_mw_ }
			# add_modifier = { modifier = pm_glyph_mw_ }
			# add_modifier = { modifier = pm_glyph_mw_ }
			# add_modifier = { modifier = pm_glyph_mw_ }
			# add_modifier = { modifier = pm_glyph_mw_ }
			# add_modifier = { modifier = pm_glyph_mw_ }
			set_capital = yes
			random_country = {
				limit = { has_country_flag = sokar_empire }
				save_global_event_target_as = sokar_empire
				species = { save_global_event_target_as = sokarSpecies }
			}
			ensure_jaffa_exist = { homeworld = "This" }
			while = {
				count = 10
				create_pop = { species = event_target:jaffaSpecies }
				last_created_pop = { set_pop_flag = init_spawn }
			}
			ensure_human_exist = { homeworld = "This" }
			while = {
				count = 10
				create_pop = { species = event_target:humanSpecies }
				last_created_pop = { set_pop_flag = init_spawn }
			}
			generate_start_buildings = yes
			generate_goauld_fleets = yes
			set_name = "Delmak"
		}	
		moon = {
			name = "Netu" # Prision Molten world, Should have some habitability to keep the population there.
			class = "pc_molten"
			size = 6
			orbit_distance = 7
			orbit_angle = 115
			has_ring = no
		}
	}
	
	planet = {
		name = "Aaru" # Field of Reeds - Egyptian Paradise related to Sokar-Ptah-Osiris
		class = "pc_gas_giant"
		orbit_distance = 25
		orbit_angle = 60
		size = 20
		has_ring = no
	}
}