@distance = 30
@base_moon_distance = 10

kali_homeworld = { 
	name = "Kali's Home system"
	class = "sc_g"
	flags = { }
	init_effect = { }
	usage = custom_empire
	max_instances = 1
	planet = {
		name = "Kali's Home Star"
		class = pc_g_star
		orbit_distance = 0
		orbit_angle = 1
		size = { min = 30 max = 35 }
	}
	planet = {
		name = "Kali's Home World"
		class = pc_desert
		orbit_distance = { min = 30 max = 30 }
		orbit_angle = { min = 40 max = 180 }
		size = { min = 10 max = 15 }
		has_ring = no
		starting_planet = yes
		deposit_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes set_planet_flag = planet_kali }
		init_effect = {
			set_global_flag = kali_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = kali_empire } } }
				create_species = { 
				    name = "Dominion of Kali" 
				    class = KAL
				    portrait = goauld
				    homeworld = THIS
				    traits = {
						trait =	"trait_extremely_adaptive"
						trait =	"trait_decadent"
						trait =	"trait_weak"
						trait =	"trait_repugnant"
						trait =	"trait_rapid_breeders"
						ideal_planet_class = "pc_desert"
					}
				}
				last_created_species = { save_global_event_target_as = kaliSpecies }
				create_country = {
					name = "NAME_kali_dominion"
					ship_prefix = "RAA"
					type = default
					origin = "default"
					ignore_initial_colony_error = yes
					civics = { civic = "civic_stargate_kali" civic = "civic_corvee_system" }
					authority = auth_dictatoric
					name_list = "ART2"
					ethos = { ethic = "ethic_fanatic_authoritarian" ethic = "ethic_pacifists" }
					species = event_target:kaliSpecies
					flag = {
						icon =	{ category =	"stargate" file =	"kali.dds" }
						background =	{ category =	"backgrounds" file =	"v.dds" }
						colors =	{ "black" "black" "null" "null" }
					}
					effect = {
						set_graphical_culture = tauri_01
						set_country_flag = kali_empire
						save_global_event_target_as = kali_empire
					}
				}
				set_owner = event_target:kali_empire
			}
			add_modifier = { modifier = pm_stargate_network_milkyway }
			# add_modifier = { modifier = pm_glyph_mw_ }
			# add_modifier = { modifier = pm_glyph_mw_ }
			# add_modifier = { modifier = pm_glyph_mw_ }
			# add_modifier = { modifier = pm_glyph_mw_ }
			# add_modifier = { modifier = pm_glyph_mw_ }
			# add_modifier = { modifier = pm_glyph_mw_ }
			set_capital = yes
			random_country = {
				limit = { has_country_flag = kali_empire }
				save_global_event_target_as = kali_empire
				species = { save_global_event_target_as = kaliSpecies }
			}
			ensure_jaffa_exist = { homeworld = "This" }
			while = {
				count = 10
				create_pop = { species = event_target:jaffaSpecies }
				last_created_pop = { set_pop_flag = init_spawn }
			}
			ensure_human_exist = { homeworld = "This" }
			while = {
				count = 10
				create_pop = { species = event_target:humanSpecies }
				last_created_pop = { set_pop_flag = init_spawn }
			}
			generate_start_buildings = yes
			generate_goauld_fleets = yes
			set_name = "Kali Homeworld"
		}	
	}
}