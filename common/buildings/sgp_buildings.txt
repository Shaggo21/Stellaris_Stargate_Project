### General Buildings ###

building_stargate_milkyway = {
	base_buildtime = 0
	prerequisites = { none }
	can_build = no
	category = stargate
	planet_modifier = { }
	icon = building_stargate_milkyway
	resources = {
		category = { }
		cost = { 0 }
		upkeep = { energy = 4 }
	}
}

building_stargate_pegasus = {
	base_buildtime = 0
	prerequisites = { none }
	can_build = no
	category = stargate
	planet_modifier = { }
	icon = building_stargate_pegasus
	resources = {
		category = { } 
		cost = { 0 }
		upkeep = { energy = 2 }
	}
}

### Goauld Specific buildings ###

building_larvae_temple_1 ={
	base_buildtime = 360
	prerequisites = { "tech_Goauld_start" }
	can_build = yes
	category = resource
	show_tech_unlock_if = { has_country_flag = parasite_goa  }
	planet_modifier = { job_larvae_priest_add = 1    }
	icon = building_larvae_temple_1
	resources = {
	   category = planet_buildings
	   cost = { minerals = 400 consumer_goods = 50 }
	   upkeep = { energy = 4 }
	}
	potential = {
	   exists = owner
	   owner = { has_country_flag = parasite_goa }
	}
	destroy_trigger = { exists = owner owner = { NOT = { has_country_flag = parasite_goa } } }
	allow = {
	   requires_capital_2 = yes
	}
	upgrades = { building_larvae_temple_2 }
	ai_weight = {
	   weight = 500000
	   modifier = {
		  factor = 0
		  owner = { has_monthly_income = { resource = consumer_goods value < 10 } }
	   }
	   modifier = {
		  factor = 0
		  free_jobs > 1
	   }
	}
 }

 ###Lord specific buildings ###

 ###Tauri specific buildings###
