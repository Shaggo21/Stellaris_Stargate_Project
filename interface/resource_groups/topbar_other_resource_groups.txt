tb_others_group = {
	resources = {
		sr_crew
		sr_naquadah
		sr_naquadria
		sr_larvae
		sr_goauls
		sr_deuterium
		sr_luxuries
		#sr_cordrazine
		#sr_duranium
		#sr_tallonian
		#sr_kemocite
		#sr_topaline
		#sr_magnesite
		#sr_boronite
		#sr_time_crystal
		#sr_trellium
		#sr_pergium
		minor_artifacts
	}
	localization = {
		"RESOURCE_GROUP_DEFAULT" = default	
		"RESOURCE_GROUP_DEFAULT_NEG_DEC" = { balance < 0 stored >= 1000 stored < 10000 }
		"RESOURCE_GROUP_DEFAULT_NEG" = { balance < 0 }
		"RESOURCE_GROUP_DEFAULT_MAX" = { max > 0 stored >= max }
		"RESOURCE_GROUP_DEFAULT_DEC" = { stored >= 1000 stored < 10000 }
	}
	onclick = market
}