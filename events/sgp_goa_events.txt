namespace = sgp_goa_events

pop_event = {
	id = sgp_goa_events.1
	hide_window = yes
	is_triggered_only = yes
	immediate = {
		save_event_target_as = joined_pop
		species = { save_event_target_as = joined_pop_species }
		join_pop = yes
		#kill_pop = yes
		#create_pop = {
			#species = owner_main_species
		#}
	}

}

# country_event = {
# 	id = sgp_goa_events.2
# 	title = sgp_goa_events.2.name
# 	desc = { text =  sgp_goa_events.2.desc }
# 	picture = {
# 		trigger = { has_country_flag = goa_empire }
# 		picture = { sgp_GFX_evt_slalliance}
# 	}
# 	is_triggered_only = yes
# 	trigger = {
# 		exists = THIS
# 		THIS = { has_country_flag = goa_empire }
# 		exists = FROM
# 		FROM = { has_leader_flag = systemlord }
# 	}
# }  Options 